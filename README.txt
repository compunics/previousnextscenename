The plugin "PreviousNextSceneName" shows the previous-/next-panoname as the respective controlbar-button tooltip.
It works with the following PTP controlbars:

classicControlBar
dockedThumbsControlBar
iControlBar
urbanControlBar
roundCornerBar
D-padControlBar
CirclesControlBar

As an alternative, you can assign your own previous/next buttons or use the default ones that come with the plugin.
The position, size, transparency and behaviour of these buttons can be set via config menu.

The plugin has been tested with V2.5.3 of PTP on Linux.
As usual: this is free software, enjoy at your own risk.
